<?php
namespace Dayone\Issuer;

class UniliverTopup {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\UniliverServiceProvider');
        return 'Uniliver::index_topup';
    }

}