<?php
namespace Dayone\Issuer;
use Dayone\Helpers\VoucherHelper;
use App\Helpers\Translate;
use App\Models\Voucher;

class UniliverAgoda {

    public function __construct(){

    }

    public function generate($data,$lang = 'vi',$create_image = false)
    {
        if(env('APP_ENV') == 'production')
        {
            try{
                $agoda_info = $this->CreateAgodaInfo(['quantity'=>1,'amount'=>$data['value']]);
                if(isset($agoda_info[0]) && isset($agoda_info[1]))
                {
                    \DB::transaction(function () use ($data,$agoda_info,&$voucher){
                        #voucher generation logic
                        $helper = new VoucherHelper();
                        $uniqueId = str_replace(array('.',' '),'',rand(1000000000,9999999999).microtime());
                        $link = $helper->_generateLink($uniqueId);
                        $data['link'] = $link;
                        $data['code'] = $agoda_info[0];
                        $data['expired_date']=date('Y-m-d',strtotime(str_replace('/','-',$agoda_info[2])));
                        $voucher = Voucher::create($data);
                    });
                    if($create_image === true)
                    {
                        if(Storage::disk('s3')->has(env('AWS_VOUCHER_FOLDER') . '/' . $voucher->link . '-' . md5($voucher->voucher_id.$voucher->code) . "_" . $lang . '.png') == false)
                        {
                            $this->voucherImg($voucher->link,md5($voucher->voucher_id.$voucher->code),$lang);
                        }
                    }
                    return $voucher;
                }
                else{
                    \Log::error('No data response form server');
                    return false;
                }
            }catch(\Exception $e){
                \Log::error($e);
                return false;
            }
        }else{

            $agoda_info = $this->CreateAgodaDemo();
            \DB::transaction(function () use ($data,$agoda_info,&$voucher){
                #voucher generation logic
                $helper = new VoucherHelper();
                $uniqueId = str_replace(array('.',' '),'',rand(1000000000,9999999999).microtime());
                $link = $helper->_generateLink($uniqueId);
                $data['link'] = $link;
                $data['code'] = $agoda_info[0];
                $data['expired_date']=date('Y-m-d',strtotime(str_replace('/','-',$agoda_info[2])));
                $voucher = Voucher::create($data);

            });
            if($create_image === true)
            {
                if(Storage::disk('s3')->has(env('AWS_VOUCHER_FOLDER') . '/' . $voucher->link . '-' . md5($voucher->voucher_id.$voucher->code) . "_" . $lang . '.png') == false)
                {
                    $this->voucherImg($voucher->link,md5($voucher->voucher_id.$voucher->code),$lang);
                }
            }
            return $voucher;
        }
       
    }


    /**
     * Display a listing of the resource.
     *
     * @param $params
     * @return \Illuminate\Http\JsonResponse
     * @author Ha Tran <manhhaniit@gmail.com>
     */
     private function CreateAgodaInfo($params)
     {

        $result = \DB::transaction(function () use ($params) {
             $results = \DB::select('select * from agoda where issued = :issued and value = :value limit 1',
             ['issued' => 0,'value' => $params['amount']]);

             if(empty($results)){//if empty data in database
                \Log::info('No data in agoda table');
                return false;
             }else{
                $affected = \DB::update('update agoda set issued = 1, issued_at = NOW() where agoda_id = ?', [$results[0]->agoda_id]);
               if($affected == 1){
                   return $results[0];
               }
             }

         });
         //dd($result);

         if(!empty($result)){
           $return_result[0] = $result->code;
           $return_result[1] = $result->value;
           $return_result[2] = $result->expiry;
           \Log::info(json_encode($return_result));

           return $return_result;
         }
           
    }

    private function CreateAgodaDemo()
    {

        $params = array(
                'amount'=>100000,
            );
   
        $result = \DB::transaction(function () use ($params) {
             $results = \DB::select('select * from agoda where issued = :issued and value = :value limit 1',
             ['issued' => 0,'value' => $params['amount']]);

             if(empty($results)){//if empty data in database
                \Log::info('No data in agoda table');
                return false;
             }else{
                $affected = \DB::update('update agoda set issued = 1, issued_at = NOW() where agoda_id = ?', [$results[0]->agoda_id]);
           
                if($affected == 1){
                   return $results[0];
               }
             }

         });
        
        if(!empty($result)){
           $return_result[0] = $result->code;
           $return_result[1] = $result->value;
           $return_result[2] = $result->expiry;
           \Log::info(json_encode($return_result));

           return $return_result;
        }
        else{
            return false;
        }

        // $result = "1|12345678|".$this->randomizeVoucherCode(10).":987654321987654:31/12/2018";
        // // Băm kết quả
        // $result = explode('|', $result);
        // // Băm lấy thông tin thẻ
        // $result = explode(':', $result[2]);
        // if ($result[0] && $result[1] && $result[2]) {
        //     return $result;
        // } else {
        //     return false;
        // }
    }

    private function randomizeVoucherCode($len = 10) {
        $chars = '0123456789';
        $clen = strlen($chars) - 1;

        $res = '';
        $msum = 0;

        for ($i = 0; $i < $len - 1; $i++) {
            $rand = $chars[mt_rand(0, $clen)];
            $res .= $rand;

            $msum += intval($rand) * (int)substr(($i + 2),0,1);
        }

        $checksum = (11 - ($msum % 11));

        if($checksum > 9){
            $checksum -= 10;
        }

        $res .= $checksum;

        return $res;
    }

    public function voucherImg($link, $token, $lang = null){
        $voucher = \DB::table('voucher as v')
            ->select(
                'v.voucher_id','i.img_id','p.product_id','p.name_vi','p.name_en','i.img_path','v.code',
                'v.expired_date','b.name_vi as brand_name','pp.size_name_vi','pp.size_name_en','pp.price',
                'p.type','v.sender_name','v.receiver_name','v.message','v.state','b.brand_id',
                \DB::raw('(select count(price_id) from product_price where product_id = p.product_id and state = 1) as count_price '),
                \DB::raw('(SELECT img_path FROM image WHERE img_id = b.logo_image) AS b_img')
            )
            ->join('product as p','p.product_id','=','v.product_id')
            ->join('product_image as pi','pi.product_id','=','p.product_id')
            ->join('image as i','i.img_id','=','pi.size3_image')
            ->join('brand as b','b.brand_id','=','p.brand_id')
            ->join('product_price as pp','pp.price_id','=','v.price_id')
            ->whereRaw("BINARY v.link = '$link'")
            ->where('pi.order','=','1')
            ->whereIn('v.state', [0,1,2,3,4,5])
            ->first();

        if($lang == null){
            $lang = Session::get('laravel_language', 'vi');
        }else{
            if(!in_array($lang, ['vi', 'en'])){
                $lang = 'vi';
            }
        }

        \App::setLocale($lang);

        if($voucher == false || md5($voucher->voucher_id.$voucher->code) != $token){
            return '';
        }

        $base = new Imagick(base_path('public/layouts/templates/new/voucher-bg-'.$lang.'.png'));
        $mask = new Imagick(base_path('public/layouts/templates/new/mask.png'));
        $gotit = new Imagick(base_path('public/layouts/templates/new/voucher-logo.png'));
        $product = new Imagick(env('IMG_SERVER').$voucher->img_path);
        $blogo = new Imagick(env('IMG_SERVER').$voucher->b_img);
        $blogo->resizeImage(120,120,Imagick::FILTER_LANCZOS, 1);

        //declare position for barcode
        if(strlen($voucher->code) == 12){
            $p_textbarcode = 133;
            $p_textspace = 10;
        }
        elseif(strlen($voucher->code) == 13){
            $p_textbarcode = 133;
            $p_textspace = 11;
        }
        elseif(strlen($voucher->code) == 14){
            $p_textbarcode = 121;
            $p_textspace = 11;
        }
        else{
            $p_barcode = 99;
            $p_textbarcode = 170;
            $p_textspace = 11;
        }

        // Setting same size for all images
        $product->resizeImage(640,640, Imagick::FILTER_LANCZOS, 1);

        // Copy opacity mask
        $base->compositeImage($mask, Imagick::COMPOSITE_DSTIN, 0, 0, Imagick::CHANNEL_ALPHA);

        // Add overlay
        $base->compositeImage($product, Imagick::COMPOSITE_DEFAULT, 0, 0);
        $base->compositeImage($blogo, Imagick::COMPOSITE_DEFAULT, 20, 653);
        $base->compositeImage($gotit, Imagick::COMPOSITE_DEFAULT, 0, 0);
        // Watermark text
        $pname = wordwrap(trim(Translate::transObj($voucher,'name', $lang)),32," \n", true);

        $expire = trans('content.validity').': '.date('d/m/Y', strtotime($voucher->expired_date));

        // Create a new drawing palette
        $draw = new \ImagickDraw();

        // Set font properties
        $draw->setFont(base_path('public/layouts/v1/assets/fonts/Roboto/Roboto-Medium.ttf'));
        $draw->setFontSize(28);

        // Draw text on the image
        $base->annotateImage($draw, 180, 685, 0, str_limit($voucher->brand_name,27));
        $draw->setFont(base_path('public/layouts/v1/assets/fonts/Roboto/Roboto-Regular.ttf'));
        $draw->setFontSize(28);

        $base->annotateImage($draw, 180, 721, 0, $pname);

        $psize = null;

        if($voucher->type == 'i') {
            $size = trans('content.price') . Translate::transObj($voucher, 'size_name', $lang);

            if(strlen(trim($size)) > 0 && $voucher->count_price > 1){
                $psize = wordwrap($size,32,"\n");
            }else{
                $psize = null;
            }
        }else if($voucher->type == 'c'){
            if($voucher->price > 0) {
                $size = number_format($voucher->price, 0, '', '.') . "đ";
                $psize = wordwrap($size,32,"\n");
            }else{
                $psize = null;
            }
        }

        if($psize != null) {
            $size_offset = (ceil(strlen($pname) / 32)) * 30;
            $base->annotateImage($draw, 180, 728 + $size_offset, 0, $psize);
        }
        $expire_space = 208;
        if($lang == 'vi'){
            $expire_space = 197;
        }
        $draw->setFontSize(22);
        $base->annotateImage($draw, $expire_space, 865, 0, $expire);
        $draw->setFontSize(20);
        $draw->settextkerning($p_textspace);
        $draw->setFontSize(28);
        $base->annotateImage($draw, $p_textbarcode, 910, 0, implode(" " , array(substr($voucher->code, 0,4),substr($voucher->code, 4,5),substr($voucher->code, 9,5) )));

        $base->setImageCompression(Imagick::COMPRESSION_NO);
        #$base->roundCorners(10, 10);//Border radius for image background
        $base->setImageCompressionQuality(80);

        file_put_contents(base_path('public/temp_img.png'), $base->getImagesBlob());
        if(ob_get_length() > 0)
            ob_end_clean();
        file_put_contents(base_path('public/temp_compress.png'), $this->compress_png(base_path('public/temp_img.png')));
        unlink(base_path('public/temp_img.png'));
        \Storage::disk('s3')->put(env('AWS_VOUCHER_FOLDER').'/'.$link.'-'.$token.'_'.$lang.'.png', file_get_contents(base_path('public/temp_compress.png')));
        unlink(base_path('public/temp_compress.png'));

        return env('AWS_VOUCHER_FOLDER').'/'.$link.'-'.$token.'_'.$lang.'.png';
    }

    private function compress_png($path_to_png_file, $max_quality = 80)
    {
        $min_quality = 30;
        $compressed_png_content = shell_exec("pngquant --quality=$min_quality-$max_quality - < ".$path_to_png_file);
        return $compressed_png_content;
    }

    public function view()
    {
        \App::register('Dayone\Issuer\UniliverServiceProvider');
        return 'Uniliver::index_agoda';
    }

}